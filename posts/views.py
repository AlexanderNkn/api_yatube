from django.core.exceptions import PermissionDenied
from rest_framework import viewsets
#from rest_framework.permissions import (SAFE_METHODS, BasePermission,
#                                        IsAuthenticated)

from .models import Comment, Post
from .serializers import CommentSerializer, PostSerializer


#class IsOwnerOrReadOnly(BasePermission):
#    '''
#    Not allow editing/deleting post or comment if the current user is not an author
#    '''
#    def has_object_permission(self, request, view, obj):
#        if request.method in SAFE_METHODS:
#            return True
#        return obj.author == request.user


class ApiPostViewSet(viewsets.ModelViewSet):
    """
    List all posts, or create a new post.
    Retrieve, update or delete selected post.
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
#    permission_classes = [IsAuthenticated & IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def perform_update(self, serializer):
        post = Post.objects.get(pk=self.kwargs.get('pk'))
        if post.author != self.request.user:
            raise PermissionDenied
        serializer.save()

    def perform_destroy(self, instance):
        if instance.author != self.request.user:
            raise PermissionDenied
        instance.delete()


class ApiCommentViewSet(viewsets.ModelViewSet):
    """
    List all comments, or create a new comment for selected post.
    Retrieve, update or delete selected comment.
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
#    permission_classes = [IsAuthenticated & IsOwnerOrReadOnly]

    def get_queryset(self):
        post_id = self.kwargs.get('post_id')
        return Comment.objects.filter(post__id=post_id)

    def perform_create(self, serializer):
        post = Post.objects.get(id=self.kwargs.get('post_id'))
        serializer.save(author=self.request.user, post=post)

    def perform_update(self, serializer):
        comment = Comment.objects.get(pk=self.kwargs.get('pk'))
        if comment.author != self.request.user:
            raise PermissionDenied
        serializer.save()

    def perform_destroy(self, instance):
        if instance.author != self.request.user:
            raise PermissionDenied
        instance.delete()
