from rest_framework import serializers

from .models import Comment, Post


class PostSerializer(serializers.ModelSerializer):
    '''posts serialization'''
    author = serializers.ReadOnlyField(source='author.username')
    class Meta:
        model = Post
        fields = ['id', 'text', 'author', 'image', 'pub_date',]


class CommentSerializer(serializers.ModelSerializer):
    '''comments serialization'''
    author = serializers.ReadOnlyField(source='author.username')
    post = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Comment
        fields = ['id', 'author', 'post', 'text', 'created',]
